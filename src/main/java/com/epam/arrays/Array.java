package com.epam.arrays;
import java.util.Arrays;
public class Array {
    public static void main(String[] args) {
        int[] A = {1, 10, 9, 6, 8, 3, 7, 8};
        int[] B = {4, 6, 15, 12, 11, 16, 17, 20, 9, 61, 7};
        Method method = new Method();
        int[] c = method.returnSameElementsArray(A, B);
        System.out.println(Arrays.toString(c));
        int[] c1 = method.returnUniqueElementsArray(A, B);
        System.out.println(Arrays.toString(c1));
    }
}
