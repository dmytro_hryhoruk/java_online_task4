package com.epam.arrays;
import java.util.Arrays;
public class Method {
    public int[] returnSameElementsArray(int[] a, int[] b) {
        int size;
        if (a.length >= b.length) {
            size = a.length;
        } else {
            size = b.length;
        }
        Arrays.sort(a);
        Arrays.sort(b);
        int[] c = new int[size];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b.length; j++) {
                if (a[i] == b[j]) {
                    c[i] = a[i];
                }
            }
        }
        return c;
    }
    public int[] returnUniqueElementsArray(int[] a, int[] b) {
        int size;
        if (a.length >= b.length) {
            size = a.length;
        } else {
            size = b.length;
        }
        Arrays.sort(a);
        Arrays.sort(b);
        int[] c = new int[size];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b.length; j++) {
                if (a[i] != b[j]) {
                    c[i] = a[i];
                }
            }
        }
        return c;
    }
}

