package com.epam.game;
public class Door {
    String name;
    public int power;
    public void create() {
        int monsterOrArtifact = 1 + (int) (Math.random() * 2);
        if (monsterOrArtifact == 1) {
            int monstersPowerPoints = (int) (Math.random() * 91) + 5;
            this.name = "Monster with " + monstersPowerPoints + " power points;";
            this.power = -(monstersPowerPoints);
        } else {
            int artifactPowerPoints = (int) (Math.random() * 71) + 10;
            this.name = "Artifact that gives you " + artifactPowerPoints + " points of power;";
            this.power = artifactPowerPoints;
        }
    }
    public void createAllDoors(Door[] doors) {
        for (int i = 0; i < doors.length; i++) {
            doors[i] = new Door();
            doors[i].create();
        }
    }
    public void showAllDoors(Door[] doors) {
        for (int i = 0; i < doors.length; i++) {
            System.out.println(i + 1 + ". " + doors[i].name);
        }
    }
    public int expectDeath(Door[] doors, Hero character, int i, int count) {
        if (i == (doors.length)) {
            return count;
        }
        if (doors[i].power < 0) {
            if (Math.abs(doors[i].power) > character.powerPoints) {
                count++;
            }
        }
        return expectDeath(doors, character, i + 1, count);
    }
}

