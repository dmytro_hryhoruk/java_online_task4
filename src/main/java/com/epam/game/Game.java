package com.epam.game;
import java.util.Scanner;
public class Game {
    public Scanner src = new Scanner(System.in);
    public Door door = new Door();
    public void startTheGame(Hero character, Door[] doors) {
        Menu.initializeEnum();
        switch (Menu.makeUserChoice()) {
            case (1):
                System.out.println("Choose the door number(from 1 to 10)");
                int choice = src.nextInt();
                if (choice < 1 || choice > 10) {
                    do {
                        System.out.println("There is no door with that number,choose again");
                        choice = src.nextInt();
                    } while (choice < 1 || choice > 10);
                }
                character.openDoor(doors[choice]);
                System.out.println("Want to continue?\n1-Yes/2-No(exit)");
                if (src.nextInt() == 1) {
                    startTheGame(character, doors);
                } else {
                    System.exit(0);
                }
                break;
            case (2):
                door.showAllDoors(doors);
                System.out.println("Now you see what's hiding behind each door.\nWant to continue?\n1-Yes/2-No(exit)");
                if (src.nextInt() == 1) {
                    startTheGame(character, doors);
                } else {
                    System.exit(0);
                }
                break;
            case (3):
                int deaths = door.expectDeath(doors, character, 0, 0);
                System.out.println(character.getName() + " will find his death behind " + deaths + " doors");
                System.out.println("Want to continue?\n1-Yes/2-No(exit)");
                if (src.nextInt() == 1) {
                    startTheGame(character, doors);
                } else {
                    System.exit(0);
                }
                break;
            case (4):
                int[] result = new int[10];
                System.out.println("To survive, you should open doors in that order: ");
                character.surviveWay = character.surviveWay(doors, character, result);
                if (character.surviveWay[0] == 0) {
                    System.out.print(character.surviveWay[0] + 1 + " ");
                }
                for (int i : character.surviveWay) {
                    if (i != 0)
                        System.out.print(i + 1 + " ");

                }
                System.out.println("Want to continue?\n1-Yes/2-No(exit)");
                if (src.nextInt() == 1) {
                    startTheGame(character, doors);
                } else {
                    System.exit(0);
                }
                break;
            case (5):
                System.out.println("See you");
                System.exit(0);
            default:
                System.out.println("There is no such option, choose again");
                startTheGame(character, doors);
                break;
        }
    }
}

