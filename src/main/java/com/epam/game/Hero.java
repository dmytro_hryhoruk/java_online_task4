package com.epam.game;

public class Hero {
    public String name;
    public int powerPoints = 25;
    int[] surviveWay;
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void openDoor(Door door) {
        int battle = door.power;
        if (battle < 0) {
            if (Math.abs(battle) > powerPoints) {
                System.out.println(name + " has been murdered. Game over.");
                this.powerPoints = 0;
            } else {
                System.out.println(name + " killed the monster.");
            }
        } else {
            this.powerPoints += battle;
            System.out.println("Hero got " + this.powerPoints + " power points");
        }
    }
    public int[] surviveWay(Door[] doors, Hero character, int[] result) {
        int power = character.powerPoints;
        int j = 0;
        for (int i = 0; i < doors.length; i++) {
            if (doors[i].power > 0) {
                power += doors[i].power;
                result[j] = i;
                j++;
            } else if (power < Math.abs(doors[i].power)) {
                continue;
            } else {
                result[j] = i;
                j++;
            }
        }
        return result;
    }
}
