package com.epam.game;
import java.util.Scanner;
public  class Main {
    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);
        Game game = new Game();
        Hero character = new Hero();
        Door door = new Door();
        Door [] doors = new Door[10];
        System.out.println("Let's play the game!!! \nGive your Hero a name");
        character.setName(src.nextLine());
        System.out.println(character.getName()+",now you're starting your adventure");
        door.createAllDoors(doors);
        game.startTheGame(character,doors);

    }
}
