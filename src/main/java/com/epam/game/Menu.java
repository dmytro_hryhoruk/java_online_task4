package com.epam.game;
import java.util.Scanner;
public enum Menu {
    OPEN_THE_DOOR("Open the door."), GET_DOORS("What/Who is behind the doors?"),
    WHERE_DEATH("Where does the hero will die?"), GET_WAY("The way to win."), EXIT("Exit");
    private String menuName;
    static Scanner src = new Scanner(System.in);
    Menu() {
    }
    Menu(String menuName) {
        this.menuName = menuName;
    }
    public String getMenuName() {
        return menuName;
    }
    public static void initializeEnum() {
        for (Menu m : Menu.values()) {
            System.out.println(m.ordinal() + 1 + ". " + m.getMenuName());
        }
    }
    public static int makeUserChoice() {
        int choice = src.nextInt();
        switch (choice) {
            case (1):
                return 1;
            case (2):
                return 2;
            case (3):
                return 3;
            case (4):
                return 4;
            case (5):
                return 5;
            default:
                System.out.println("There is no such an option, choose again");
                return makeUserChoice();
        }
    }
}
